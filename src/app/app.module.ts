import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PostComponent } from './post-list/post.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NewpostComponent } from './newpost/newpost.component';
import { PostlistComponent } from './postlist/postlist.component';
import { PostService } from './services/post.service';

const appRoutes: Routes = [
  { path: 'postlist', component: PostlistComponent },
  { path: 'newpost', component: NewpostComponent },
  { path: '', component: PostlistComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    HeaderComponent,
    NewpostComponent,
    PostlistComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
