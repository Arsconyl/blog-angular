import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../models/Post.model';
import {Subscription} from 'rxjs';
import {PostService} from '../services/post.service';

@Component({
  selector: 'app-postlist',
  templateUrl: './postlist.component.html',
  styleUrls: ['./postlist.component.scss']
})
export class PostlistComponent implements OnInit, OnDestroy {
  posts: Post[];
  postSubscription: Subscription;

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postSubscription = this.postService.postSubject.subscribe(
      (users: Post[]) => {
        this.posts = users;
      }
    );
    this.postService.emitPosts();
  }
  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }

  Like(id: number)
  {
    this.postService.likePost(id);
  }
  Dislike(id: number)
  {
    this.postService.dislikePost(id);
  }
  Delete(id: number)
  {
    this.postService.delPost(id);
    let i;
    if (id > -1) {
      this.posts.splice(id, 1);
      for (i=0; i <this.posts.length; i++)
      {
        this.posts[i].id=i;
      }
    }
  }

  getColor(id: number)
  {
    return this.postService.getColor(id);
  }
}
